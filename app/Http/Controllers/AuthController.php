<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function submit(Request $request){
        $namaawal = $request ['namaawal'];
        $namaakhir = $request ['namaakhir'];
        return view('welcome', compact('namaawal','namaakhir'));
    }
}
